<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_donneesVent extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function insert_donneesVent($prmArrayData)
    {

       	$this->db->insert('mesvent', $prmArrayData);  //On insére le tableau reçu en paramètre 
        $last_id = $this->db->insert_id() ;		//On renvoie le dernière ID insérer dans la table 
        return $this->select_detailMesvent($last_id) ;	//On récupère les données du dernier enregistrement avec son ID

    }

    public function insert_donneesAir($prmArrayData)
    {

       	$this->db->insert('mesair', $prmArrayData);
        $last_id = $this->db->insert_id() ;
        return $this->select_detailMesair($last_id) ;

    }

    public function select_detailMesvent($prmid)
    {
        
        $query = $this->db->select('*')
                          ->from('mesvent')
                          ->where('IDvent', $prmid)
                          ->get();
        return $query->result_array();
    }

    public function select_detailMesair($prmid)
    {
        
        $query = $this->db->select('*')
                          ->from('mesair')
                          ->where('IDair', $prmid)
                          ->get();
        return $query->result_array();
    }

		

}