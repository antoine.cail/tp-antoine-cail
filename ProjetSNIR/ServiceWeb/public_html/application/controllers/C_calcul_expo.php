<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_calcul_expo extends CI_Controller {
    public  function __construct() {
        parent::__construct();
        $this->load->model('M_calcul_expo');
         $this->load->helper("url");
    }

    public function index_nom_plage($prmID)
    {
        $data['titre'] = "La liste des plages de la communes";
        $data['Titre_Page'] = "Les plages";
        
        $array_resultat = $this->M_calcul_expo->select_nom_plage($prmID);
        $data['result'] = $array_resultat;

        $array_resultats = $this->M_calcul_expo->select_indice_uv($prmID);
        $data['resultat'] = $array_resultats;

        $page = $this->load->view('V_calcul_expo_soleil', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page));
    }

    public function index_detail_peau($prmID)
    {
        $data['titre'] = "La liste des plages de la communes";
        $data['Titre_Page'] = "Les plages";

        $array_resultat = $this->M_calcul_expo->select_nom_plage($prmID);
        $data['result'] = $array_resultat;

        $page = $this->load->view('V_detail_peau', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page));
    }


}