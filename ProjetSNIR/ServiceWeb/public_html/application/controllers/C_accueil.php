<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_accueil extends CI_Controller {
    public  function __construct() {
        parent::__construct();
        $this->load->model('M_indiceUv');
         $this->load->helper("url");
    }
    public function index()
    {
        $data['titre'] = "La liste des plages de la communes";
        $data['Titre_Page'] = "Les plages";
       
        $array_resultat = $this->M_indiceUv->select_all_MessagePerso();
        $data['result'] = $array_resultat;
        $page = $this->load->view('V_indiceUv', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page)); 
    }

    public function index_liste_plage()
    {
        $data['titre'] = "La liste des plages de la communes";
        $data['Titre_Page'] = "Les plages";

        $array_resultat = $this->M_indiceUv->select_liste_plage();
        $data['result'] = $array_resultat;
        $page = $this->load->view('V_liste_plage', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page));
    }
    public function index_get()
    {
        $array_resultat = $this->M_indiceUv->select_all();
        $data['result'] = $array_resultat;
    }

    public function index_Message_get()
    {
    }
}