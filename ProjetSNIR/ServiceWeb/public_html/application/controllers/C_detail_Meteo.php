<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_detail_Meteo extends CI_Controller {
    public  function __construct() {
        parent::__construct();
        $this->load->model('M_detail_Meteo');
         $this->load->helper("url");
    }

    public function index_liste_plage()
    {
        $data['titre'] = "La liste des plages de la communes";
        $data['Titre_Page'] = "Les plages";

        $array_resultat = $this->M_indiceUv->select_liste_plage();
        $data['result'] = $array_resultat;
        $page = $this->load->view('V_liste_plage', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page));
    }

    public function index_nom_plage($prmNom)
    {
        $array_resultat = $this->M_detail_Meteo->select_nom_plage($prmNom);
        $data['result'] = $array_resultat;
        $data['titre'] = "La liste des plages de la communes";

        $page = $this->load->view('V_plage_meteo', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page));
    }

    public function index_temp_eau($prmEau)
    {
        $array_resultat = $this->M_detail_Meteo->select_temp_eau($prmEau);
        $data['result'] = $array_resultat;
        $data['titre'] = "La liste des plages de la communes";

        $page = $this->load->view('V_plage_meteo', $data, true);
        $this->load->view('commun/V_template', array('contenu' => $page));
    }
}