<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php' ;

class C_donneesVent extends REST_Controller {
        
        public  function __construct() {
                parent::__construct();
                $this->load->model('M_donneesVent');                    //Chargement du model "M_donneesVent"
            }
            
            public function index_post()   
            {                
                    $dto = $this->_post_args['dto'] ;  //Récupération de la variable dto (POST)
                       
                    $dto = (array) json_decode($dto) ; //Conversion du JSON en tableau (array)

		    //Tableau à insérer dans la table mesvent
	 	    $prmDataVent = [
			"vitesse" => $dto['vitesse'],
			"direction" => $dto['direction'],
			"IDplage" => $dto['IDplage'] 
		    ] ; 

		    //Tableau à insérer dans la table mesair
		    $prmDataAir = [
			"temperature" => $dto['temperature'],
			"IDplage" => $dto['IDplage']
		    ] ;	

		    //Insertion des données dans la table mesvent
                    $resultDataVent = $this->M_donneesVent->insert_donneesVent($prmDataVent) ;

		    //Insertion des données dans la table mesair
		    $resultDataAir = $this->M_donneesVent->insert_donneesAir($prmDataAir) ;

		    //Tableau des valeurs que l'on reçoit
		    $results = array(
			"dataVent" => $resultDataVent[0],
			"dataAir" => $resultDataAir[0]
		    );
                    
                    //Afficher ce que le service Web renvoit (ce qu'on a enregistré)
                    $this->response($results, REST_Controller::HTTP_CREATED) ;
            } 
}

