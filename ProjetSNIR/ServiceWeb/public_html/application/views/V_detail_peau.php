<div class="card border-warning mb-3 mt-5 rounded mx-auto d-block" style="max-width: 50rem; max-height: 50rem;">
    <div class="card-header display-4 font-italic">
        <center>Quel est votre type de peau ? </center>
    </div>
</div>
<section class="ml-5">
    <div class="ml-5">
        <div class="card ml-5 mt-5" style="width: 40rem;">
            <img src="<?php echo base_url("assets/images/phototype1.png"); ?>" class="card-img-top" alt="phototype1" style="width : 150px; margin: auto;">
            <div class="card-body">
                <p class="card-text" style="font-size : 2em;">La peau est très blanche, les cheveux roux ou blonds, les yeux bleus / verts. Les tâches de rousseur son fréquentes.</p>
                <p class="card-text font-italic" style="font-size : 2em;"><u>Les coups de soleil sont systématiques, la peau rougit toujours mais ne bronze jamais.</u></p>
            </div>
        </div>
    </div>
    <div class="ml-5">
        <div class="card ml-5 mt-5" style="width: 40rem;">
            <img src="<?php echo base_url("assets/images/phototype2.png"); ?>" class="card-img-top" alt="phototype2" style="width : 150px; margin: auto;">
            <div class="card-body">
                <p class="card-text" style="font-size : 2em;">La peau est claire, les cheveux blonds/ roux à châtains, le yeux claire à bruns. Des tâches de rousseur peuvent apparaître.</p>
                <p class="card-text font-italic" style="font-size : 2em;"><u>Les coups de soleil sont fréquents et la peau bronze à peine ou très lentement.</u></p>
            </div>
        </div>
    </div>
    <div class="ml-5">
        <div class="card ml-5 mt-5" style="width: 40rem;">
            <img src="<?php echo base_url("assets/images/phototype3.png"); ?>" class="card-img-top" alt="phototype3" style="width : 150px; margin: auto;">
            <div class="card-body">
                <p class="card-text" style="font-size : 2em;">La peau est intermédiaire, les cheveux sont châtains à bruns et les yeux bruns</p>
                <p class="card-text font-italic" style="font-size : 2em;"><u>Les coups de soleil sont occasionnels. La peau bronze graduellement.</u></p>
            </div>
        </div>
    </div>
    <div class="ml-5">
        <div class="card ml-5 mt-5" style="width: 40rem;">
            <img src="<?php echo base_url("assets/images/phototype4.png"); ?>" class="card-img-top" alt="phototype4" style="width : 150px; margin: auto;">
            <div class="card-body">
                <p class="card-text" style="font-size : 2em;">La peau est mate, les cheveux bruns / noirs, les yeux bruns / noirs. </p>
                <p class="card-text font-italic" style="font-size : 2em;"><u>La peau bronze rapidement, avec des coups de soleil occasionnels lors d'exposition intenses.</u></p>
            </div>
        </div>
    </div>
    <div class="ml-5">
        <div class="card ml-5 mt-5" style="width: 40rem;">
            <img src="<?php echo base_url("assets/images/phototype5.png"); ?>" class="card-img-top" alt="phototype5" style="width : 150px; margin: auto;">
            <div class="card-body">
                <p class="card-text" style="font-size : 2em;">La peau est brune, les cheveux et les yeux sont noirs.</p>
                <p class="card-text font-italic" style="font-size : 2em;"><u>La peau bronze beaucoup. Les coups de soleil sont rares.</u></p>
            </div>
        </div>
    </div>
    <div class="ml-5">
        <div class="card ml-5 mt-5" style="width: 40rem;">
            <img src="<?php echo base_url("assets/images/phototype6.png"); ?>" class="card-img-top" alt="phototype6" style="width : 150px; margin: auto;">
            <div class="card-body">
                <p class="card-text" style="font-size : 2em;">La peau est noire, les cheveux et les yeux sont noirs</p>
                <p class="card-text font-italic" style="font-size : 2em;"><u>Les coups de soleil sont très exceptionnels.</u></p>
            </div>
        </div>
    </div>
</section>
<?php foreach ($result as $row) {
?>
    <a class="btn btn-warning ml-3 font-italic font-weight-bold" href="<?php echo site_url("C_calcul_expo/index_nom_plage"); ?>/<?= $row['IDplage']; ?>" role="button">Retour</a>
<?php
    }
?>