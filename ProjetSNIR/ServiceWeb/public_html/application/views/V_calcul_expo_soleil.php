</nav>
<section class="ml-5 mt-5">
    <div class="card border-warning mb-3 mt-5 rounded mx-auto d-block" style="max-width: 50rem; max-height: 50rem;">
        <div class="card-header display-4 font-italic">
            <center>Plage de <?php echo $result[0]['nom']; ?> </center>
        </div>
    </div>
    <p class="display-4 font-weight-bold font-italic">
        <img src="<?php echo base_url("assets/images/indiceUV.jpg"); ?>" width="280" height="270" alt="tempEau" class="ml-5">
        : <?php echo $resultat[0]['iuv']; ?>
    </p>
    <div class="card border-warning mb-3 mt-3 rounded mx-auto d-block" style="max-width: 50rem; max-height: 50rem;">
        <div class="card-header display-4 font-italic">
            <center>Votre type de peau :<a class="btn btn-warning ml-3 font-italic font-weight-bold" href="<?php echo base_url("C_calcul_expo/index_detail_peau"); ?>" role="button">?</a> </center>
        </div>
    </div>
    <div class="display-4 ml-5">
        <div class="form-check form-check-inline display-4 ">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            <label class="form-check-label" for="inlineRadio1"><img src="<?php echo base_url("assets/images/phototype1.png"); ?>" style="max-width: 150px; max-height: 50rem;"></label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
            <label class="form-check-label" for="inlineRadio2"><img src="<?php echo base_url("assets/images/phototype2.png"); ?>" style="max-width: 150px; max-height: 50rem;"></label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
            <label class="form-check-label" for="inlineRadio3"><img src="<?php echo base_url("assets/images/phototype3.png"); ?>" style="max-width: 150px; max-height: 50rem;"></label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option3">
            <label class="form-check-label" for="inlineRadio4"><img src="<?php echo base_url("assets/images/phototype4.png"); ?>" style="max-width: 150px; max-height: 50rem;"></label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="option3">
            <label class="form-check-label" for="inlineRadio5"><img src="<?php echo base_url("assets/images/phototype5.png"); ?>" style="max-width: 150px; max-height: 50rem;"></label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio6" value="option3">
            <label class="form-check-label" for="inlineRadio6"><img src="<?php echo base_url("assets/images/phototype6.png"); ?>" style="max-width: 150px; max-height: 50rem;"></label>
        </div>
    </div>

    <div class="card border-warning mb-3 mt-3 rounded mx-auto d-block" style="max-width: 50rem; max-height: 50rem;">
        <div class="card-header display-4 font-italic">
            <center>Votre âge :</center>
        </div>
    </div>
    <div class="ml-5 mt-4 input-group-lg" style="max-width: 350px;">
            <input type="text" class="form-control ml-5" placeholder="Age" style="font-size: 3em;">
    </div>
</section>