</nav>
<div class="card border-warning mb-3 mt-5 rounded mx-auto d-block" style="max-width: 50rem; max-height: 50rem;">
    <div class="card-header display-4 font-italic">
        <center>La méteo de la plage de <?php echo $resultat[0]['nom']; ?> </center>
    </div>
</div>
<section class="ml-5">
    <p class="display-4 font-weight-bold font-italic">
        <img src="<?php echo base_url("assets/images/TempEau.jpg"); ?>" width="280" height="270" alt="tempEau" class="ml-5">
        : <?php echo $result[0]['temperature']; ?>°C
    </p>
    <br />
    <p class="display-4 font-weight-bold font-italic">
        <img src="<?php echo base_url("assets/images/TempAir.jpg"); ?>" width="280" height="270" alt="tempEau" class="ml-5">
        : <?php echo $results[0]['temperature']; ?>°C
    </p>
    <br />
    <p class="display-4 font-weight-bold font-italic">
        <img src="<?php echo base_url("assets/images/DirectionVent.png"); ?>" width="280" height="270" alt="tempEau" class="ml-5">
        : <?php echo $resultats[0]['vitesse']; ?> km/h en
        <p class="display-4 font-weight-bold font-italic ml-5">direction du <?php echo $resultats[0]['direction']; ?></p>
    </p>
    <div class="btn-liste ml-5 mt-3 mb-5">
        <?php foreach ($resultat as $row) {
        ?>

            <a href="<?php echo site_url("C_calcul_expo/index_nom_plage"); ?>/<?= $row['IDplage']; ?> " class="btn btn-outline-warning btn-lg active ml-5 mb-3 mt-5" role="button" aria-pressed="true">
                <h1>Votre temps d'exposition au soleil</h1>
            </a>
        <?php
        }
        ?>
    </div>
</section>