 </nav>
 <div class="card border-warning mb-3 mt-5 rounded mx-auto d-block" style="max-width: 50rem; max-height: 50rem;">
     <div class="card-header display-4">Message de l'office du tourisme</div>
     <div class="card-body text-warning">
         <h5 class="card-title display-4"><?php echo $result[0]['message']; ?></h5>
     </div>
 </div>
 <div class="btn-liste ml-5 mt-5">
     <a href="<?php echo site_url("C_accueil/index_liste_plage"); ?>" class="btn btn-outline-warning btn-lg active ml-5 mb-3 mt-5" role="button" aria-pressed="true">
         <h1>La liste des plages</h1>
     </a>
 </div>
     