<html>

<head>
    <title><?php echo $titre; ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    <!-- Image and text -->
    
    <nav class="navbar navbar-light" style="background-image: radial-gradient(circle at top left, orange, yellow);">
        <a class="navbar-brand" href="#">
            <img src="<?php echo base_url("assets/images/logo_Client.png"); ?>" width="200" height="270" alt="" class="rounded mx-auto d-block">
            <h1 class="display-3 font-weight-bold font-italic ml-5">La méteo des plages</h1>
        </a>
    <section>
        <?php echo $contenu; ?>
    </section>
</body>

</html>