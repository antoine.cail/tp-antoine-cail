//Inclure le fichier suivant:
const traitementData = require('./traitementData');

//Modules chargés
const SerialPort = require('serialport');//Chargement du module serialport
const Readline = require('@serialport/parser-readline'); //Chargement du module parser-readline

//---------Déclarations de var globales-------:
//Variable permettant de récuperer les mesures des différentes trames
var mesureVitesse = 0;
var mesureTemp = 0;
var mesureDirection = 0;

//Création d'un objet de type SerialPort
//Paramètre : descripteur de péripherique actif sur le rasp et vitesse de transmission normalisé NMEA0183
const port = new SerialPort('/dev/ttyUSB0', { baudRate: 4800 });

//Délimiter la trame reçu (1 trame = 1 ligne), délimiter par le retour chariot et saut de ligne
const parser = port.pipe(new Readline({ delimiter: '\r\n' }));

parser.on('data', readSerialData);

function readSerialData(data) {                         //fonction de callback, appelé à chaque fois qu'une trame est reçu

  if (data.indexOf("$II") > -1) {                       //Si la trame commence par II
    let trameVent = data.split(',');                    //Permet de diviser la trame à partir du séparateur ","

    if (trameVent.length === 6) {                       //Verifie que le tableau a bien 6 sous-chaînes
      mesureVitesse = parseFloat(trameVent[3]);         //Sélectionne la vitesse du vent dans le tableau
      mesureVitesse *= 1.852;                            //Convertir les noeuds en km/h
      mesureVitesse = Math.round(mesureVitesse);         //Arrondir à l'unité près
      mesureDirection = parseFloat(trameVent[1]);       //Sélectionne la direction du vent dans le tableau
    }
  }

  if (data.indexOf("$WI") > -1) {                      //Si la trame commence par WI
    let trameTemperature = data.split(',');            //Permet de diviser la trame à partir du séparateur ","
    if (trameTemperature.length == 6) {                //verifie que le tableau a bien 6 sous-chaînes
      mesureTemp = parseFloat(trameTemperature[2]);    //Sélectionne la temperature du vent dans le tableau
    }
  }
}

//Interval entre chaque exécution de la fonction lireMesure
setInterval(lireMesure, 1000);

//Déclaration de la fonction lireMesure
function lireMesure() {
  traitementData.traitementAngle(mesureDirection, mesureVitesse, mesureTemp);
}