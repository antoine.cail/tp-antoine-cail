const request = require('request');     //Module permettant les requêtes http
const config = require('./config');     //Fichier de config

//------------- Délarationn des variables globales ---------
var Xrad = 0;
var Yrad = 0;
var moyVitesse = 0;
var moyTemp = 0;
var Xmoyen = 0;
var Ymoyen = 0;
var directionMoyen = 0;
var angleDegre = 0;
var cpt = 0;

//Fonction récupérant les mesures en paramètres pour les exploiter par la suite
function traitementAngle(mesureDirection, mesureVitesse, mesureTemp) {

    if (cpt < 4) {
        cpt++;
        //console.log("compteur : " + cpt);

        moyVitesse += mesureVitesse;
        moyTemp += mesureTemp;

        //console.log(moyVitesse);
        //console.log(moyTemp);

        //Conversion de ° en rad/s
        var directionRadian = mesureDirection * (Math.PI / 180);
        Xrad += (mesureVitesse * Math.cos(directionRadian));
        Yrad += (mesureVitesse * Math.sin(directionRadian));

        //console.log("x : " + Xrad + " y : " + Yrad);
    }

    if (cpt == 4) {
        //--------------- Moyenne vitesse et température + Xmoy et Ymoy -------
        moyVitesse /= cpt;
        moyTemp /= cpt;
        Xmoyen = Xrad / cpt;
        Ymoyen = Yrad / cpt;

        //console.log("Moyenne vitesse : " + moyVitesse);
        //console.log("Moyenne température : " + moyTemp);

        //--------- Determiner un angle en fonction d'un x et d'un y --------
        if (Xmoyen < 0) {        //Si X de la direction moyenne est négative alors
            directionMoyen = (Math.PI + Math.atan(Ymoyen / Xmoyen));

        } else if (Ymoyen < 0) {  //si Y de la direction moyenne est négative alors
            directionMoyen = (2 * Math.PI + Math.atan(Ymoyen / Xmoyen));

        } else {
            directionMoyen = Math.atan(Ymoyen / Xmoyen);
        }
        //---------- Si il y a du vent, on fait la conversion de rad/s à ° ---------
        if (isNaN(directionMoyen)) {
            console.log("Aucun vent à l'horizon ! ");

        } else {
            //Conversion de rad/s en °
            angleDegre = directionMoyen * (180 / Math.PI);

            //console.log("Angle en degres : " + Math.round(angleDegre));

            //---------- Determiner d'où vient le vent à partir de l'angle moyen en ° ------
            if (angleDegre >= 337.5 || angleDegre < 22.5) {
                angleDegre = "Nord";
            }

            if (angleDegre >= 22.5 && angleDegre < 67.5) {
                angleDegre = "Nord-Est";
            }

            if (angleDegre >= 67.5 && angleDegre < 112.5) {
                angleDegre = "Est";
            }

            if (angleDegre >= 112.5 && angleDegre < 157.5) {
                angleDegre = "Sud-Est"
            }

            if (angleDegre >= 157.5 && angleDegre < 202.5) {
                angleDegre = "Sud";
            }

            if (angleDegre >= 202.5 && angleDegre < 247.5) {
                angleDegre = "Sud-Ouest";
            }

            if (angleDegre >= 247.5 && angleDegre < 292.5) {
                angleDegre = "Ouest"
            }

            if (angleDegre >= 292.5 && angleDegre < 337.5) {
                angleDegre = "Nord-Ouest";
            }
        }

        //Requête POST, envoie de données vers la BDD
        request.post({
            headers: {'content-type': 'application/x-www-form-urlencoded'},
            url: `http://${config.rest.host}/surveillanceBaignade/REST/C_donneesVent/index/`,
            body: `dto={"IDplage": ${config.rest.idPlage},"vitesse": "${moyVitesse}", "temperature": "${moyTemp}", "direction": "${angleDegre}"}`
        }, (error, response, body) => console.log(body));


        //console.log(angleDegre);
        //console.log("----------------------------------------------------");
        
        //------------- Réinitialisation des variables moyennes --------
        cpt = 0;
        moyVitesse = 0;
        moyTemp = 0;
        Xrad = 0;
        Yrad = 0;
        Xmoyen = 0;
        Ymoyen = 0;
        directionMoyen = 0;
        angleDegre = 0;

    }

}

//Permet d'utiliser les fonctions de ce fichier dans un autre fichier
module.exports = {
    traitementAngle,
}

